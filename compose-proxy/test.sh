#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./test.sh <PORT>"
	exit 1
fi

if [ -x "$(command -v wget)" ] ; then
	REQUEST="wget -qO-"
elif [ -x "$(command -v curl)" ] ; then
	REQUEST="curl -s"
else
	echo "Please install wget or curl to make http requests"
	exit 3
fi

TEST_PORT=$1
FIRST=$($REQUEST localhost:$TEST_PORT)
SECOND=$($REQUEST localhost:$TEST_PORT)

if [ "$FIRST" = "$SECOND" ] ; then
	echo "First: '$FIRST'"
	echo "Second: '$SECOND'"
	echo "Consecutive requests matched"
	echo "Failed"
	exit 2
else 
	echo "OK"
fi
